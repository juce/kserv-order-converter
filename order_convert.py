#!/usr/bin/env python3

import os
import sys


def write_file_contents(pathname, outf):
    with open(pathname, 'rt') as f:
        for line in f:
            line = line.replace('\xef\xbb\xbf','')
            outf.write(line)


def process_dir(pathname):
    items = os.listdir(pathname)
    order_files = []
    gk_order_files = []
    for item in items:
        if os.path.isdir(pathname + '/' + item):
            process_dir(pathname + '/' + item)
        elif item.endswith('gk_order.txt'):
            gk_order_files.append(item)
        elif item.endswith('order.txt'):
            order_files.append(item)
    order_files.sort()
    gk_order_files.sort()

    if order_files:
        order_ini = pathname + '/order.ini'
        with open(order_ini, 'wt') as f:
            for name in order_files:
                if name == 'order.txt':
                    write_file_contents(pathname + '/' + name, f)
                    f.write('\n')
            for name in order_files:
                if name != 'order.txt':
                    section = name[:-len('_order.txt')]
                    f.write(f'\n[{section}]\n')
                    write_file_contents(pathname + '/' + name, f)
                    f.write('\n')
        print(f'created ini-file: {order_ini}')

    if gk_order_files:
        gk_order_ini = pathname + '/gk_order.ini'
        with open(gk_order_ini, 'wt') as f:
            for name in gk_order_files:
                if name == 'gk_order.txt':
                    write_file_contents(pathname + '/' + name, f)
                    f.write('\n')
            for name in gk_order_files:
                if name != 'gk_order.txt':
                    section = name[:-len('_gk_order.txt')]
                    f.write(f'\n[{section}]\n')
                    write_file_contents(pathname + '/' + name, f)
                    f.write('\n')
        print(f'created ini-file: {gk_order_ini}')
         

if __name__ == "__main__":
    if len(sys.argv)<2:
        print(f'Usage: {sys.argv[0]} <starting-dir>')
        sys.exit(0)

    starting_dir = sys.argv[1]
    process_dir(starting_dir)

